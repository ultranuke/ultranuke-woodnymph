﻿using System;

namespace UltraNuke.WoodNymph
{
    public class Converter
    {
        #region Public Methods
        public static dynamic ChangeTo(dynamic source, Type dest)
        {
            return Convert.ChangeType(source, dest);
        }
        #endregion
    }
}
