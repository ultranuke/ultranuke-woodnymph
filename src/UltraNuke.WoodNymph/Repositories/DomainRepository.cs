﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.WoodNymph.Domain;
using UltraNuke.WoodNymph.EventHandling;
using UltraNuke.WoodNymph.EventStore;
using System.Transactions;

namespace UltraNuke.WoodNymph.Repositories
{
    public class DomainRepository : DisposableObject, IDomainRepository
    {
        #region Private Fields
        private readonly ISnapshotStore snapshotStore;
        private readonly IEventStore eventStore;
        private readonly IEventBus eventBus;
        #endregion

        #region Constructors
        public DomainRepository()
        {
            snapshotStore = ServiceLocator.Current.GetInstance<ISnapshotStore>();
            eventStore = ServiceLocator.Current.GetInstance<IEventStore>();
            eventBus = ServiceLocator.Current.GetInstance<IEventBus>();
        }
        #endregion

        #region Protected Methods
        protected TAggregateRoot CreateAggregateRootInstance<TAggregateRoot>()
            where TAggregateRoot : class, IAggregateRoot
        {
            Type aggregateRootType = typeof(TAggregateRoot);
            ConstructorInfo constructor = aggregateRootType
                .GetConstructors(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance)
                .Where(p =>
                {
                    var parameters = p.GetParameters();
                    return parameters == null || parameters.Length == 0;
                }).FirstOrDefault();

            if (constructor != null)
                return constructor.Invoke(null) as TAggregateRoot;

            throw new RepositoryException(string.Format("At least one parameterless constructor should be defined on the aggregate root type '{0}'.", typeof(TAggregateRoot).FullName));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.snapshotStore.Dispose();
                this.eventStore.Dispose();
                this.eventBus.Dispose();
            }
        }
        #endregion

        #region IRepository Members
        public void Save<TAggregateRoot>(TAggregateRoot aggregateRoot) where TAggregateRoot : class, Domain.IAggregateRoot
        {
            if (this.snapshotStore.CanAppendSnapshot(aggregateRoot))
            {
                this.snapshotStore.AppendSnapshot(aggregateRoot);
            }
            IEnumerable<IDomainEventMessage> events = aggregateRoot.UncommittedEvents;
            foreach (IDomainEventMessage @event in events)
            {
                this.eventStore.AppendEvent(@event);
                this.eventBus.Publish(@event);
            }
        }

        public TAggregateRoot Get<TAggregateRoot>(Guid id) where TAggregateRoot : class, Domain.IAggregateRoot
        {
            TAggregateRoot aggregateRoot = this.CreateAggregateRootInstance<TAggregateRoot>();
            if (this.snapshotStore != null && this.snapshotStore.HasSnapshot(typeof(TAggregateRoot), id))
            {
                ISnapshot snapshot = snapshotStore.ReadSnapshot(typeof(TAggregateRoot), id);
                aggregateRoot.BuildFromSnapshot(snapshot);
                var eventsAfterSnapshot = this.eventStore.ReadEvents(typeof(TAggregateRoot), id, snapshot.Version);
                if (eventsAfterSnapshot != null && eventsAfterSnapshot.Count() > 0)
                    aggregateRoot.BuildFromHistory(eventsAfterSnapshot);
            }
            else
            {
                aggregateRoot.Id = id;
                var evnts = this.eventStore.ReadEvents(typeof(TAggregateRoot), id);
                if (evnts != null && evnts.Count() > 0)
                    aggregateRoot.BuildFromHistory(evnts);
                else
                    throw new RepositoryException(string.Format("The aggregate (id={0}) cannot be found in the repository.", id));
            }
            return aggregateRoot;
        }
        #endregion
    }
}
