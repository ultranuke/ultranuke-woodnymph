﻿using System;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.Repositories
{
    public interface IDomainRepository : IDisposable
    {
        #region Methods
        void Save<TAggregateRoot>(TAggregateRoot aggregateRoot)
            where TAggregateRoot : class, IAggregateRoot;

        TAggregateRoot Get<TAggregateRoot>(Guid id)
            where TAggregateRoot : class, IAggregateRoot;
        #endregion
    }
}
