﻿using System.Collections.Generic;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventHandling
{
    public interface IEventHandlerFactory
    {
        #region Methods
        IEnumerable<IEventHandler<TDomainEventMessage>> GetHandlers<TDomainEventMessage>() where TDomainEventMessage : IDomainEventMessage;
        #endregion
    }
}
