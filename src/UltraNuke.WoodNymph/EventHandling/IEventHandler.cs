﻿using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventHandling
{
    public interface IEventHandler<TDomainEventMessage> where TDomainEventMessage : IDomainEventMessage
    {
        #region Methods
        void Handle(TDomainEventMessage @event);
        #endregion
    }
}
