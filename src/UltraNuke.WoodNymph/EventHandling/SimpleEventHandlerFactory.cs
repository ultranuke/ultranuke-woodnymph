﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventHandling
{
    public class SimpleEventHandlerFactory : IEventHandlerFactory
    {
        #region Private Methods
        private IEnumerable<Type> GetHandlerTypes<TDomainEventMessage>() where TDomainEventMessage : IDomainEventMessage
        {
            IEnumerable<object> allInstances = ServiceLocator.Current.GetAllInstances(null);
            IEnumerable<Type> handlers;
            handlers = allInstances
                .Select(p=>p.GetType())
                    .Where(x => x.GetInterfaces()
                        .Any(a => a.IsGenericType && a.GetGenericTypeDefinition() == typeof(IEventHandler<>)))
                        .Where(h => h.GetInterfaces()
                            .Any(ii => ii.GetGenericArguments()
                                .Any(aa => aa == typeof(TDomainEventMessage)))).ToList();
            return handlers;
        }
        #endregion

        #region IEventHandlerFactory Members
        public IEnumerable<IEventHandler<TDomainEventMessage>> GetHandlers<TDomainEventMessage>() where TDomainEventMessage : IDomainEventMessage
        {
            var handlers = GetHandlerTypes<TDomainEventMessage>();
            var eventHandlers = handlers.Select(handler =>
                (IEventHandler<TDomainEventMessage>)ServiceLocator.Current.GetInstance(handler)).ToList();
            return eventHandlers;
        }
        #endregion        
    }
}
