﻿using System;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventHandling
{
    public interface IEventBus : IDisposable
    {
        #region Methods
        void Publish<TDomainEventMessage>(TDomainEventMessage @event) where TDomainEventMessage : IDomainEventMessage;
        #endregion
    }
}
