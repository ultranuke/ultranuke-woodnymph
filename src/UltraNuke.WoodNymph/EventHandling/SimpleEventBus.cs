﻿using System.Collections.Generic;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventHandling
{
    public class SimpleEventBus : DisposableObject, IEventBus
    {
        #region Private Methods
        private void DoPublish<TDomainEventMessage>(TDomainEventMessage @event) where TDomainEventMessage : IDomainEventMessage
        {
            IEventHandlerFactory handlerFactory = ServiceLocator.Current.GetInstance<IEventHandlerFactory>();
            IEnumerable<IEventHandler<TDomainEventMessage>> handlers = handlerFactory.GetHandlers<TDomainEventMessage>();
            foreach (IEventHandler<TDomainEventMessage> handler in handlers)
            {
                handler.Handle(@event);
            }
        }
        #endregion

        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
        }
        #endregion

        #region IEventBus Members
        public void Publish<TDomainEventMessage>(TDomainEventMessage @event) where TDomainEventMessage : IDomainEventMessage
        {
            var desEvent = Converter.ChangeTo(@event, @event.GetType());
            DoPublish(desEvent);
        }
        #endregion
    }
}
