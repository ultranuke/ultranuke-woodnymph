﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke.WoodNymph.Domain
{
    [Serializable]
    public class Snapshot : ISnapshot
    {
        #region ISnapshot Members
        public string SnapshotType { get; set; }
        public DateTime Timestamp { get; set; }
        public int Version { get; set; }
        public Guid AggregateRootId { get; set; }
        public string AggregateRootType { get; set; }
        #endregion

        #region IEntity Members
        public Guid Id { get; set; }
        #endregion
    }
}
