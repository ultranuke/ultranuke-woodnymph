﻿namespace UltraNuke.WoodNymph.Domain
{
    public interface IDomainEventHandle<TDomainEventMessage> where TDomainEventMessage : IDomainEventMessage
    {
        #region Methods
        void Handle(TDomainEventMessage e);
        #endregion
    }
}
