﻿using System;

namespace UltraNuke.WoodNymph.Domain
{
    public interface IEntity
    {
        #region Properties
        Guid Id { get; set; }
        #endregion
    }
}
