﻿using System;
using System.Collections.Generic;
using System.Linq;
using UltraNuke.WoodNymph.EventHandling;

namespace UltraNuke.WoodNymph.Domain
{
    public abstract class AggregateRoot : IAggregateRoot
    {
        #region Private Fields
        private Guid id;
        private int version = 0;
        private int eventVersion;
        private readonly List<IDomainEventMessage> uncommittedEvents = new List<IDomainEventMessage>();
        #endregion

        #region Constructors
        public AggregateRoot()
            : this(Guid.NewGuid())
        {
        }

        public AggregateRoot(Guid id)
        {
            this.id = id;
            this.eventVersion = this.version;
        }
        #endregion

        #region Private Methods
        private void HandleEvent<TDomainEventMessage>(TDomainEventMessage @event)
            where TDomainEventMessage : IDomainEventMessage
        {
            dynamic d = this;
            d.Handle(Converter.ChangeTo(@event, @event.GetType()));
        }
        #endregion

        #region Protected Methods
        protected virtual void RaiseEvent<TDomainEventMessage>(TDomainEventMessage @event) where TDomainEventMessage : IDomainEventMessage
        {
            @event.Id = Guid.NewGuid();
            @event.DomainEventType = typeof(TDomainEventMessage).AssemblyQualifiedName;
            @event.Timestamp = DateTime.UtcNow;
            @event.Version = ++eventVersion;
            @event.AggregateRootId = this.Id;
            @event.AggregateRootType = this.GetType().AssemblyQualifiedName;
            this.HandleEvent<TDomainEventMessage>(@event);
            uncommittedEvents.Add(@event);
        }
        protected abstract ISnapshot DoCreateSnapshot();
        protected abstract void DoBuildFromSnapshot(ISnapshot snapshot);
        #endregion

        #region IAggregateRoot Members
        public void BuildFromHistory(IEnumerable<IDomainEventMessage> histories)
        {
            if (this.uncommittedEvents.Count() > 0)
                this.uncommittedEvents.Clear();
            foreach (IDomainEventMessage @event in histories)
                this.HandleEvent<IDomainEventMessage>(@event);
            this.version = histories.Last().Version;
            this.eventVersion = this.version;
        }

        public IEnumerable<IDomainEventMessage> UncommittedEvents
        {
            get { return uncommittedEvents; }
        }

        public int Version
        {
            get { return this.version + uncommittedEvents.Count; ; }
        }
        #endregion

        #region IEntity Members
        public virtual Guid Id
        {
            get { return this.id; }
            set { this.id = value; }
        }
        #endregion

        #region ISnapshotOrignator Members
        public void BuildFromSnapshot(ISnapshot snapshot)
        {
            this.version = snapshot.Version;
            this.id = snapshot.AggregateRootId;
            DoBuildFromSnapshot(snapshot);
            this.uncommittedEvents.Clear();
        }

        public ISnapshot CreateSnapshot()
        {
            ISnapshot snapshot = this.DoCreateSnapshot();
            snapshot.Id = Guid.NewGuid();
            snapshot.Version = this.Version;
            snapshot.Timestamp = DateTime.UtcNow;
            snapshot.AggregateRootId = this.id;
            return snapshot;
        }
        #endregion
    }
}
