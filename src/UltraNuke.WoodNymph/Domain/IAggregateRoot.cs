﻿using System.Collections.Generic;
using UltraNuke.WoodNymph.EventHandling;

namespace UltraNuke.WoodNymph.Domain
{
    public interface IAggregateRoot : IEntity, ISnapshotOrignator
    {
        #region Properties
        IEnumerable<IDomainEventMessage> UncommittedEvents { get; }
        int Version { get; }
        #endregion

        #region Methods
        void BuildFromHistory(IEnumerable<IDomainEventMessage> historicalEvents);
        #endregion
    }
}
