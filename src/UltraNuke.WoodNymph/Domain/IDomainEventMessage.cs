﻿using System;

namespace UltraNuke.WoodNymph.Domain
{
    public interface IDomainEventMessage : IEntity
    {
        #region Properties
        string DomainEventType { get; set; }
        DateTime Timestamp { get; set; }
        int Version { get; set; }
        Guid AggregateRootId { get; set; }
        string AggregateRootType { get; set; }
        #endregion
    }
}
