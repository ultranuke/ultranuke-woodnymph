﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke.WoodNymph.Domain
{
    public interface ISnapshotOrignator
    {
        #region Methods
        void BuildFromSnapshot(ISnapshot snapshot);

        ISnapshot CreateSnapshot();
        #endregion
    }
}
