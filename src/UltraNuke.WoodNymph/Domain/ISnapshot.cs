﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UltraNuke.WoodNymph.Domain
{
    public interface ISnapshot : IEntity
    {
        #region Properties
        string SnapshotType { get; set; }
        DateTime Timestamp { get; set; }
        int Version { get; set; }
        Guid AggregateRootId { get; set; }
        string AggregateRootType { get; set; }
        #endregion
    }
}
