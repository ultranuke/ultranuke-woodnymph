﻿using System;
using UltraNuke.WoodNymph.Utility;

namespace UltraNuke.WoodNymph.Domain
{
    [Serializable]
    public class DomainEventMessage : IDomainEventMessage
    {

        #region Private Constants
        private const int InitialPrime = 23;
        private const int FactorPrime = 29;
        #endregion

        #region Public Methods
        public override int GetHashCode()
        {
            return Utils.GetHashCode(this.AggregateRootId.GetHashCode(),
                this.Id.GetHashCode(),
                this.Timestamp.GetHashCode(),
                this.Version.GetHashCode());
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(this, obj))
                return true;
            if (obj == null)
                return false;
            DomainEventMessage other = obj as DomainEventMessage;
            if ((object)other == (object)null)
                return false;
            return this.Id == other.Id;
        }
        #endregion

        #region IDomainEventMessage Members
        public virtual string DomainEventType { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual int Version { get; set; }
        public virtual Guid AggregateRootId { get; set; }
        public virtual string AggregateRootType { get; set; }
        #endregion

        #region IEntity Members
        public virtual Guid Id { get; set; }
        #endregion
    }
}
