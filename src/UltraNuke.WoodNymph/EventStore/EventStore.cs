﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.Data;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventStore
{
    public class EventStore : DisposableObject, IEventStore
    {
        #region Private Fields
        private readonly IDbConnection cnn = null;
        private readonly IData data = null;
        #endregion

        #region Constructors
        public EventStore(IDbConnection cnn)
        {
            this.cnn = cnn;
            this.data = Data;
        }
        #endregion

        #region Private Properties
        private IData Data
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IData>();
            }
        }
        #endregion

        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
        }
        #endregion

        #region IEventStore Members
        public void AppendEvent(IDomainEventMessage @event)
        {
            string sql = @"insert into DomainEvents(Id,DomainEventType,DomainEventData,Timestamp,Version,AggregateRootId,AggregateRootType) 
                                                  values (@Id,@DomainEventType,@DomainEventData,@Timestamp,@Version,@AggregateRootId,@AggregateRootType)";
            DomainEventDataObject dataObject = DomainEventDataObject.FromEvent(@event);
            data.Execute(cnn, sql, dataObject);
        }

        public IEnumerable<IDomainEventMessage> ReadEvents(Type aggregateRootType, Guid aggregateRootId)
        {
            string sql = @"select * from DomainEvents 
                    where AggregateRootType = @AggregateRootType and AggregateRootId = @AggregateRootId
                    order by Version";

            IEnumerable<DomainEventDataObject> dataObjects = data.Query<DomainEventDataObject>(cnn, sql, 
                new { AggregateRootType = aggregateRootType.AssemblyQualifiedName, AggregateRootId = aggregateRootId });

            return dataObjects.Select(p => p.ToEvent());
        }

        public IEnumerable<IDomainEventMessage> ReadEvents(Type aggregateRootType, Guid aggregateRootId, int version)
        {
            string sql = @"select * from DomainEvents 
                    where AggregateRootType = @AggregateRootType and AggregateRootId = @AggregateRootId 
                        and Version > @Version
                    order by Version";

            IEnumerable<DomainEventDataObject> dataObjects = data.Query<DomainEventDataObject>(cnn, sql,
                                                                new
                                                                {
                                                                    AggregateRootType = aggregateRootType.AssemblyQualifiedName,
                                                                    AggregateRootId = aggregateRootId,
                                                                    Version = version
                                                                });
            return dataObjects.Select(p => p.ToEvent());
        }
        #endregion
    }
}
