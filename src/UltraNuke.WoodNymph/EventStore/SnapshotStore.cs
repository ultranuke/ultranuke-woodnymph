﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using NHibernate;
using NHibernate.Criterion;
using UltraNuke.Data;
using UltraNuke.WoodNymph.Domain;
using Microsoft.Practices.ServiceLocation;

namespace UltraNuke.WoodNymph.EventStore
{
    public class SnapshotStore : DisposableObject, ISnapshotStore
    {
        #region Private Fields
        private readonly IDbConnection cnn = null;
        private readonly IData data = null;
        private readonly int numOfEvents;
        private readonly Dictionary<string, ISnapshot> snapshotMapping = new Dictionary<string, ISnapshot>();
        #endregion

        #region Constructors
        public SnapshotStore(IDbConnection cnn)
            : this(100, cnn)
        {
        }

        public SnapshotStore(int numOfEvents, IDbConnection cnn)
        {
            this.numOfEvents = numOfEvents;
            this.cnn = cnn;
            this.data = Data;
        }
        #endregion

        #region Private Properties
        private IData Data
        {
            get
            {
                return ServiceLocator.Current.GetInstance<IData>();
            }
        }
        #endregion

        #region Protected Methods
        protected override void Dispose(bool disposing)
        {
        }
        #endregion

        #region ISnapshotStore Members
        public bool CanAppendSnapshot(IAggregateRoot aggregateRoot)
        {
            Type aggregateRootType = aggregateRoot.GetType();
            Guid aggregateRootId = aggregateRoot.Id;
            int version = aggregateRoot.Version;

            if (HasSnapshot(aggregateRootType, aggregateRootId))
            {
                ISnapshot snapshot = ReadSnapshot(aggregateRootType, aggregateRootId);
                return snapshot.Version + numOfEvents <= aggregateRoot.Version;
            }
            else
            {
                string sql = @"select Version from Snapshots 
                    where AggregateRootType = @AggregateRootType and AggregateRootId = @AggregateRootId
                        and Version <= @Version";

                int count = data.Query<int>(cnn, sql,
                                                    new
                                                    {
                                                        AggregateRootType = aggregateRootType.AssemblyQualifiedName,
                                                        AggregateRootId = aggregateRootId,
                                                        Version = version
                                                    }).Count();

                return count >= this.numOfEvents;
            }
        }

        public void AppendSnapshot(IAggregateRoot aggregateRoot)
        {
            string sql = @"insert into Snapshots(Id,SnapshotType,SnapshotData,Timestamp,Version,AggregateRootId,AggregateRootType) 
                                                  values (@Id,@SnapshotType,@SnapshotData,@Timestamp,@Version,@AggregateRootId,@AggregateRootType)";
            SnapshotDataObject dataObject = SnapshotDataObject.CreateFromAggregateRoot(aggregateRoot);
            data.Execute(cnn, sql, dataObject);
        }

        public bool HasSnapshot(Type aggregateRootType, Guid aggregateRootId)
        {
            string sql = @"select Version from Snapshots 
                    where AggregateRootType = @AggregateRootType and AggregateRootId = @AggregateRootId";


            var eventCount = data.Query<int>(cnn, sql,
                                                    new
                                                    {
                                                        AggregateRootType = aggregateRootType.AssemblyQualifiedName,
                                                        AggregateRootId = aggregateRootId
                                                    }).Count();

            return eventCount > 0;
        }

        public ISnapshot ReadSnapshot(Type aggregateRootType, Guid aggregateRootId)
        {
            string sql = @"select * from Snapshots 
                    where AggregateRootType = @AggregateRootType and AggregateRootId = @AggregateRootId
                    order by Version";

            IEnumerable<SnapshotDataObject> dataObjects = data.Query<SnapshotDataObject>(cnn, sql,
                new { AggregateRootType = aggregateRootType.AssemblyQualifiedName, AggregateRootId = aggregateRootId });

            return dataObjects.Select(p => p.ToSnapshot()).FirstOrDefault();
        }
        #endregion
    }
}
