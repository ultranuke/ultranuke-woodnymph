﻿using System;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventStore
{
    public interface ISnapshotStore : IDisposable
    {
        #region Methods
        bool CanAppendSnapshot(IAggregateRoot aggregateRoot);

        void AppendSnapshot(IAggregateRoot aggregateRoot);

        bool HasSnapshot(Type aggregateRootType, Guid aggregateRootId);

        ISnapshot ReadSnapshot(Type aggregateRootType, Guid aggregateRootId);
        #endregion
    }
}
