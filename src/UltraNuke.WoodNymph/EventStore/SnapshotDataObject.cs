﻿using System;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.Serialization;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventStore
{
    public class SnapshotDataObject
    {
        #region Public Properties
        public virtual Guid Id { get; set; }
        public virtual string SnapshotType { get; set; }
        public virtual byte[] SnapshotData { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual int Version { get; set; }
        public virtual Guid AggregateRootId { get; set; }
        public virtual string AggregateRootType { get; set; }
        #endregion

        #region Private Methods
        private static ISerializer GetSerializer()
        {            
            return ServiceLocator.Current.GetInstance<ISerializer>();
        }
        #endregion

        #region Public Methods
        public static SnapshotDataObject CreateFromAggregateRoot(IAggregateRoot aggregateRoot)
        {
            ISerializer serializer = GetSerializer();

            ISnapshot snapshot = aggregateRoot.CreateSnapshot();

            return new SnapshotDataObject
            {
                Id = snapshot.Id,
                SnapshotType = snapshot.GetType().AssemblyQualifiedName,
                Timestamp = snapshot.Timestamp,
                Version = aggregateRoot.Version,
                SnapshotData = serializer.Serialize(snapshot),
                AggregateRootId = aggregateRoot.Id,
                AggregateRootType = aggregateRoot.GetType().AssemblyQualifiedName
            };
        }

        public virtual ISnapshot ToSnapshot()
        {
            if (this.SnapshotData == null || this.SnapshotData.Length == 0)
                throw new ArgumentNullException("Data");

            ISerializer serializer = GetSerializer();
            ISnapshot ret = serializer.Deserialize<ISnapshot>(this.SnapshotData);
            ret.Id = this.Id;
            return ret;
        }
        #endregion
    }
}
