﻿using System;
using Microsoft.Practices.ServiceLocation;
using UltraNuke.Serialization;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventStore
{
    public class DomainEventDataObject
    {
        #region Public Properties
        public virtual Guid Id { get; set; }
        public virtual string DomainEventType { get; set; }
        public virtual byte[] DomainEventData { get; set; }
        public virtual DateTime Timestamp { get; set; }
        public virtual int Version { get; set; }
        public virtual Guid AggregateRootId { get; set; }
        public virtual string AggregateRootType { get; set; }
        #endregion

        #region Private Methods
        private static ISerializer GetSerializer()
        {            
            return ServiceLocator.Current.GetInstance<ISerializer>();
        }
        #endregion

        #region Public Methods
        public static DomainEventDataObject FromEvent(IDomainEventMessage @event)
        {
            ISerializer serializer = GetSerializer();
            DomainEventDataObject obj = new DomainEventDataObject();
            obj.Id = @event.Id;
            if (string.IsNullOrEmpty(@event.DomainEventType))
                obj.DomainEventType = @event.GetType().AssemblyQualifiedName;
            else
                obj.DomainEventType = @event.DomainEventType;

            obj.Timestamp = @event.Timestamp;
            obj.Version = @event.Version;
            obj.DomainEventData = serializer.Serialize(@event);
            obj.AggregateRootId = @event.AggregateRootId;
            obj.AggregateRootType = @event.AggregateRootType;
            return obj;
        }

        public virtual IDomainEventMessage ToEvent()
        {
            if (this.DomainEventData == null || this.DomainEventData.Length == 0)
                throw new ArgumentNullException("Data");

            ISerializer serializer = GetSerializer();
            IDomainEventMessage ret = serializer.Deserialize<IDomainEventMessage>(this.DomainEventData);
            ret.Id = this.Id;
            return ret;
        }
        #endregion
    }
}
