﻿using System;
using System.Collections.Generic;
using UltraNuke.WoodNymph.Domain;

namespace UltraNuke.WoodNymph.EventStore
{
    public interface IEventStore : IDisposable
    {
        #region Methods
        void AppendEvent(IDomainEventMessage @event);

        IEnumerable<IDomainEventMessage> ReadEvents(Type aggregateRootType, Guid aggregateRootId);

        IEnumerable<IDomainEventMessage> ReadEvents(Type aggregateRootType, Guid aggregateRootId, int version);
        #endregion
    }
}
