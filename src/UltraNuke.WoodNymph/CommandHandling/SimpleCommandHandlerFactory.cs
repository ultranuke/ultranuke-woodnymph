﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ServiceLocation;

namespace UltraNuke.WoodNymph.CommandHandling
{
    public class SimpleCommandHandlerFactory : ICommandHandlerFactory
    {
        #region Private Methods
        private IEnumerable<Type> GetHandlerTypes<TCommandMessage>() where TCommandMessage : ICommandMessage
        {
            IEnumerable<object> allInstances = ServiceLocator.Current.GetAllInstances(null);
            IEnumerable<Type> handlers;
            handlers = allInstances
                .Select(p => p.GetType())
                    .Where(x => x.GetInterfaces()
                        .Any(a => a.IsGenericType && a.GetGenericTypeDefinition() == typeof(ICommandHandler<>)))
                        .Where(h => h.GetInterfaces()
                            .Any(ii => ii.GetGenericArguments()
                                .Any(aa => aa == typeof(TCommandMessage)))).ToList();
            return handlers;
        }
        #endregion

        #region ICommandHandlerFactory Members
        public ICommandHandler<TCommandMessage> GetHandler<TCommandMessage>() where TCommandMessage : ICommandMessage
        {
            var handlers = GetHandlerTypes<TCommandMessage>();
            var cmdHandler = handlers.Select(handler =>
                (ICommandHandler<TCommandMessage>)ServiceLocator.Current.GetInstance(handler)).FirstOrDefault();
            return cmdHandler;
        }
        #endregion
    }
}
