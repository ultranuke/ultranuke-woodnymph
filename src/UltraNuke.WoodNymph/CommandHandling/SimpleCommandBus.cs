﻿using Microsoft.Practices.ServiceLocation;

namespace UltraNuke.WoodNymph.CommandHandling
{
    public class SimpleCommandBus : ICommandBus
    {
        #region Private Methods
        private void DoDispatch<TCommandMessage>(TCommandMessage command)
            where TCommandMessage : ICommandMessage
        {
            ICommandHandlerFactory handlerFactory = ServiceLocator.Current.GetInstance<ICommandHandlerFactory>();
            ICommandHandler<TCommandMessage> handler = handlerFactory.GetHandler<TCommandMessage>();
            handler.Handle(command);
        }
        #endregion

        #region ICommandBus Members
        public void Dispatch<TCommandMessage>(TCommandMessage command) 
            where TCommandMessage : ICommandMessage
        {
            var desMessage = Converter.ChangeTo(command, command.GetType());
            DoDispatch(desMessage);
        }
        #endregion
    }
}
