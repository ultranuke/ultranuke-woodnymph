﻿
namespace UltraNuke.WoodNymph.CommandHandling
{
    public interface ICommandBus
    {
        #region Methods
        void Dispatch<TCommandMessage>(TCommandMessage command) where TCommandMessage: ICommandMessage;
        #endregion
    }
}
