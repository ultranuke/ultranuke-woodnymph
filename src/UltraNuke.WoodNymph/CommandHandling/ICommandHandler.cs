﻿
namespace UltraNuke.WoodNymph.CommandHandling
{
    public interface ICommandHandler<TCommandMessage> where TCommandMessage : ICommandMessage
    {
        #region Methods
        void Handle(TCommandMessage command);
        #endregion
    }
}
