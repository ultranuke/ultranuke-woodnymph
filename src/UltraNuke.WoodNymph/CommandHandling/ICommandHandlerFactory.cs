﻿
namespace UltraNuke.WoodNymph.CommandHandling
{
    public interface ICommandHandlerFactory
    {
        #region Methods
        ICommandHandler<TCommandMessage> GetHandler<TCommandMessage>() where TCommandMessage : ICommandMessage;
        #endregion
    }
}
